package me.mitja.bond.security;

import me.mitja.bond.model.User;
import me.mitja.bond.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SpringSecurityCurrentUserHolder implements CurrentUserHolder {
    private final UserService userService;

    @Autowired
    public SpringSecurityCurrentUserHolder(UserService userService) {
        this.userService = userService;
    }

    @Override
    public User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return userService
                .findUserByEmail(auth.getName())
                .orElseThrow(() -> {
                    throw new InsufficientAuthenticationException("Unknown user");
                });
    }
}
