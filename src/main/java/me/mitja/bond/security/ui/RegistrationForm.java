package me.mitja.bond.security.ui;

import me.mitja.bond.model.User;
import me.mitja.bond.validator.FieldsValueMatch;

import javax.validation.constraints.NotEmpty;

@FieldsValueMatch(field = "password", fieldMatch = "passwordRepeat", message = "Passwords do not match!")
public class RegistrationForm {
    @NotEmpty
    private String email;

    @NotEmpty
    private String password;

    @NotEmpty
    private String passwordRepeat;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    User buildUser() {
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        return user;
    }
}
