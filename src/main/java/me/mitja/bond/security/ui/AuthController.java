package me.mitja.bond.security.ui;

import me.mitja.bond.model.User;
import me.mitja.bond.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static org.springframework.security.web.context.HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY;

@Controller
public class AuthController {

    private final UserService userService;

    @Autowired
    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String login() {
        return "auth/login";
    }

    @GetMapping("/registration")
    public String registration(RegistrationForm registrationForm, BindingResult bindingResult) {
        return "auth/registration";
    }

    @PostMapping("/registration")
    public String createNewUser(HttpServletRequest request, @Valid RegistrationForm registrationForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "auth/registration";
        }
        return userService.findUserByEmail(registrationForm.getEmail()).map(existingUser -> {
            bindingResult.rejectValue(
                    "email",
                    "error.user",
                    "There is already a user registered with the email provided"
            );
            return "auth/registration";
        }).orElseGet(() -> {
            User user = registrationForm.buildUser();
            userService.saveUser(user);
            try {
                request.login(registrationForm.getEmail(), registrationForm.getPassword());
                SecurityContext sc = SecurityContextHolder.getContext();
                sc.setAuthentication(
                        new UsernamePasswordAuthenticationToken(
                                registrationForm.getEmail(),
                                registrationForm.getPassword()
                        )
                );
                HttpSession session = request.getSession(true);
                session.setAttribute(SPRING_SECURITY_CONTEXT_KEY, sc);
            } catch (ServletException e) {
                bindingResult.rejectValue("email", "error.user", "Something went wrong");
                return "auth/registration";
            }
            return "redirect:certificates";
        });
    }
}
