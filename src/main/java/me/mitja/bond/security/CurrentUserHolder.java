package me.mitja.bond.security;

import me.mitja.bond.model.User;

public interface CurrentUserHolder {
    User getCurrentUser();
}
