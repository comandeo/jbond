package me.mitja.bond.repository;

import me.mitja.bond.model.Certificate;
import me.mitja.bond.model.CertificateType;
import me.mitja.bond.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface CertificateRepository extends PagingAndSortingRepository<Certificate, Long> {
    Optional<Certificate> findByNumberAndType(String number, CertificateType type);

    @Query("SELECT c FROM Certificate c WHERE c.status <> me.mitja.bond.model.CertificateStatus.DEAD")
    List<Certificate> findAllAlive();

    Page<Certificate> findByUsers(User user, Pageable pageable);
}
