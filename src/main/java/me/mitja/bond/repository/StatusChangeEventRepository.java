package me.mitja.bond.repository;

import me.mitja.bond.model.StatusChangeEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusChangeEventRepository extends CrudRepository<StatusChangeEvent, Long> {

}
