package me.mitja.bond.parser;

import me.mitja.bond.model.CertificateStatus;

abstract class FipsParser implements Parser {

    CertificateStatus parseStatus(String text) {
        if (text.contains("прекратил действие, но может быть восстановлен")) {
            return CertificateStatus.INVALID_BUT_CAN_BE_RESTORED;
        } else if (text.contains("не действует") || text.contains("прекратил действие")) {
            return CertificateStatus.DEAD;
        } else if (text.contains("действует")) {
            return CertificateStatus.VALID;
        } else {
            return CertificateStatus.UNKNOWN;
        }
    }

}
