package me.mitja.bond.parser;

import me.mitja.bond.model.CertificateStatus;

public interface Parser {
	CertificateStatus getStatus(String number);
}
