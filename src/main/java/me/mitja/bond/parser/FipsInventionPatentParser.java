package me.mitja.bond.parser;

import me.mitja.bond.model.CertificateStatus;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

public class FipsInventionPatentParser extends FipsParser {

	private static final Logger logger = LoggerFactory.getLogger(FipsInventionPatentParser.class);

	private static final String dbName = "RUPAT";

    private static final String cssQuery = "table.Status td#StatusR";

    private final FipsDocumentFetcher documentFetcher;

    FipsInventionPatentParser(FipsDocumentFetcher documentFetcher) {
        this.documentFetcher = documentFetcher;
    }

    @Override
	public CertificateStatus getStatus(String number) {
		try {
			Document document = documentFetcher.fetchHtmlDocument(number, dbName);
            return Optional.ofNullable(document.select(cssQuery).first())
                    .map(Element::text)
                    .map(this::parseStatus)
                    .orElseGet(() -> {
                        logger.warn("Cannot get status for Russian Invention Patent {}", number);
                        logger.warn(document.body().text());
                        return CertificateStatus.UNKNOWN;
                    });
		} catch (IOException e) {
            logger.warn("Cannot get status for Russian Invention Patent {}", number, e);
			return CertificateStatus.UNKNOWN;
		}
	}
}
