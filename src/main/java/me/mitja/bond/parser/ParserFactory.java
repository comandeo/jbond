package me.mitja.bond.parser;

import me.mitja.bond.model.CertificateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
 public class ParserFactory {


	private final Map<CertificateType, Parser> parserMap;

	@Autowired
	public ParserFactory(FipsDocumentFetcher documentFetcher) {
		parserMap = new HashMap<>();
		parserMap.put(CertificateType.RU_PATENT, new FipsInventionPatentParser(documentFetcher));
		parserMap.put(CertificateType.RU_TRADEMARK, new FipsTrademarkParser(documentFetcher));
		parserMap.put(CertificateType.EA_PATENT, new EapoParser());
	}

	@NonNull
	public Parser createParser(CertificateType certificateType) {
		return parserMap.get(certificateType);
	}
}
