package me.mitja.bond.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Component
public class FipsDocumentFetcherImpl implements FipsDocumentFetcher {

    private static final Logger logger = LoggerFactory.getLogger(FipsParser.class);

    private static final String url = "http://www1.fips.ru/fips_servl/fips_servlet";

    private static final String[] userAgents = {
            "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36 OPR/38.0.2220.41",
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246"
    };

    private static final Random random = new Random();

    @Override
    public Document fetchHtmlDocument(String number, String dbName) throws IOException {
        logger.info("Downloading info for certificate {}", number);
        Map<String, String> queryData = new HashMap<>();
        queryData.put("DB", dbName);
        queryData.put("typeFile", "html");
        queryData.put("DocNumber", number);
        return Jsoup
                .connect(url)
                .userAgent(getUserAgent())
                .data(queryData)
                .post();
    }

    private static String getUserAgent() {
        return userAgents[random.nextInt(userAgents.length)];
    }
}

