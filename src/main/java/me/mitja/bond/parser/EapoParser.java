package me.mitja.bond.parser;

import me.mitja.bond.model.CertificateStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class EapoParser implements Parser {

	private static final Logger logger = LoggerFactory.getLogger(EapoParser.class);

	private final String urlTemplate = "http://www.eapo.org/ru/patents/reestr/xpatent.php?id=";

	@Override
	public CertificateStatus getStatus(String number) {
		String result = null;

		Document document;
		try {
			logger.info("Downloading info for certificate {}", number);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			document = db.parse(getUrl(number).openStream());
			document.getDocumentElement().normalize();
			NodeList payYears = document.getElementsByTagName("pay-year");
			result = "";
			for (int i = 0; i < payYears.getLength(); i++) {
				Node yearNode = payYears.item(i);
				if (yearNode.getNodeType() == Node.ELEMENT_NODE) {
					Element yearElement = (Element) yearNode;
					NodeList yearNumbers = yearElement.getElementsByTagName("year-number");
					Element firstYearNumber = (Element) yearNumbers.item(0);
					result += firstYearNumber.getFirstChild().getNodeValue();
					result += " ";
				}

			}

		} catch (ParserConfigurationException | SAXException | IOException e) {
			logger.warn("Unexpected xml parser error", e);
		}
		// FIXME: stub
		return CertificateStatus.UNKNOWN;
	}

	private URL getUrl(String number) throws MalformedURLException {
		return new URL(urlTemplate + number);
	}

}
