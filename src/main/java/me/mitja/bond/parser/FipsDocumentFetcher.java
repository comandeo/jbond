package me.mitja.bond.parser;

import org.jsoup.nodes.Document;

import java.io.IOException;

public interface FipsDocumentFetcher {
    Document fetchHtmlDocument(String number, String dbName) throws IOException;
}
