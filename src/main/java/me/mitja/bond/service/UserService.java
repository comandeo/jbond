package me.mitja.bond.service;

import me.mitja.bond.model.Role;
import me.mitja.bond.model.User;
import me.mitja.bond.repository.RoleRepository;
import me.mitja.bond.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

@Service("userService")
public class UserService {

    private UserRepository userRepository;

    private RoleRepository roleRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private final JavaMailSender emailSender;

    @Autowired
    public UserService(UserRepository userRepository,
                       RoleRepository roleRepository,
                       BCryptPasswordEncoder bCryptPasswordEncoder,
                       JavaMailSender emailSender) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.emailSender = emailSender;
    }

    public Optional<User> findUserByEmail(String email) {
        return userRepository.findOneByEmail(email);
    }

    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1);
        Role userRole = roleRepository.findByRole("USER");
        user.setRoles(new HashSet<>(Collections.singletonList(userRole)));
        userRepository.save(user);
    }

    @Async
    public void requestMaxCertificates(User user, int maxCertificates) {
        final String subject = "Certificate number increase request";
        String body = "User " +
                user.getEmail() +
                " wants to increase max certificates to " +
                maxCertificates;
        final SimpleMailMessage message = new SimpleMailMessage();
        message.setTo("dmitry.rybakov@gmail.com");
        message.setSubject(subject);
        message.setText(body);
        emailSender.send(message);
    }

}
