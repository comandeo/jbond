package me.mitja.bond.service;

import me.mitja.bond.model.Certificate;
import me.mitja.bond.model.CertificateStatus;
import me.mitja.bond.model.StatusChangeEvent;
import me.mitja.bond.repository.StatusChangeEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatusChangeHandler {

    private final StatusChangeEventRepository repository;

    @Autowired
    public StatusChangeHandler(StatusChangeEventRepository repository) {
        this.repository = repository;
    }

    void handleStatusChange(Certificate certificate, CertificateStatus newStatus) {
        if (certificate.getStatus().equals(newStatus)) {
            return;
        }
        if (certificate.getStatus() == CertificateStatus.UNKNOWN) {
            return;
        }
        final StatusChangeEvent event = StatusChangeEvent.create(certificate, newStatus);
        repository.save(event);
    }
}
