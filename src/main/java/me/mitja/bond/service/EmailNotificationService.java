package me.mitja.bond.service;

import me.mitja.bond.model.StatusChangeEvent;
import me.mitja.bond.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EmailNotificationService {

    private final JavaMailSender emailSender;

    private final MessageSource messageSource;

    @Autowired
    public EmailNotificationService(JavaMailSender emailSender, MessageSource messageSource) {
        this.emailSender = emailSender;
        this.messageSource = messageSource;
    }

    public void sendStatusChangeNotifications(Map<User, List<StatusChangeEvent>> eventMap) {
        eventMap.forEach((user, statusChangeEvents) -> {
            final String subject = messageSource.getMessage("email.change_event.subject", null, user.getLocale());
            final String body = buildMessage(user, statusChangeEvents);
            final SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(user.getEmail());
            message.setSubject(subject);
            message.setText(body);
            emailSender.send(message);
        });
    }

    private String buildMessage(User user, List<StatusChangeEvent> events) {
        final Locale locale = user.getLocale();
        return events
                .stream()
                .map(event -> {
                    final String[] args = {
                            event.getCertificate().getNumber(),
                            messageSource.getMessage("certificate.status." + event.getOldStatus(), null, locale),
                            messageSource.getMessage("certificate.status." + event.getNewStatus(), null, locale)
                    };
                    return messageSource.getMessage("email.change_event", args, locale);
                })
                .collect(Collectors.joining("\n"));
    }
}
