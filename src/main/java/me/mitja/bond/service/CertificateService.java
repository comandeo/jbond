package me.mitja.bond.service;

import me.mitja.bond.exception.InvalidCertificate;
import me.mitja.bond.exception.TooManyCertificatesException;
import me.mitja.bond.job.CertificateStatusJobScheduler;
import me.mitja.bond.model.Certificate;
import me.mitja.bond.model.CertificateStatus;
import me.mitja.bond.model.CertificateType;
import me.mitja.bond.model.User;
import me.mitja.bond.repository.CertificateRepository;
import me.mitja.bond.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CertificateService {

    private static final Logger logger = LoggerFactory.getLogger(CertificateService.class);

    private final
    CertificateRepository certificateRepository;

    private final
    UserRepository userRepository;

    private final
    CertificateStatusJobScheduler scheduler;

    private final
    StatusChangeHandler statusChangeHandler;

    @Autowired
    public CertificateService(
        CertificateRepository certificateRepository,
        UserRepository userRepository,
        CertificateStatusJobScheduler scheduler, StatusChangeHandler statusChangeHandler) {
        this.certificateRepository = certificateRepository;
        this.userRepository = userRepository;
        this.scheduler = scheduler;
        this.statusChangeHandler = statusChangeHandler;
    }

    // Get

    public Optional<Certificate> getIfAlive(Long id) {
        return certificateRepository.findById(id).filter(Certificate::isAlive);
    }

    public Page<Certificate> getCertificates(User user, int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return certificateRepository.findByUsers(user, pageable);
    }

    // Create

    @Transactional
    public void createCertificate(User user, String certificateNumber, CertificateType certificateType) throws TooManyCertificatesException, InvalidCertificate {
        if (user.getCertificates().size() >= user.getMaxCertificates()) {
            throw new TooManyCertificatesException();
        }
        Certificate certificate = findOrCreate(certificateNumber, certificateType);
        user.addCertificate(certificate);
        certificateRepository.save(certificate);
        userRepository.save(user);
        scheduler.scheduleGetStatusJob(certificate);
    }

    @Transactional
    public void createCertificates(User user, List<String> certificateNumbers, CertificateType certificateType) throws TooManyCertificatesException {
        // FIXME: do not load all certs of a user here
        if (user.getCertificates().size() + certificateNumbers.size() > user.getMaxCertificates()) {
            throw new TooManyCertificatesException();
        }
        List<Certificate> certificates = certificateNumbers
                .stream()
                .map(certificateNumber -> {
                    Certificate certificate = findOrCreate(certificateNumber, certificateType);
                    user.addCertificate(certificate);
                    return certificate;
                })
                .collect(Collectors.toList());
        Iterable<Certificate> savedCertificates = certificateRepository.saveAll(certificates);
        userRepository.save(user);
        StreamSupport
                .stream(savedCertificates.spliterator(), false)
                .forEach(scheduler::scheduleGetStatusJob);
    }

    // Other
    @Transactional
    public void updateStatus(Certificate certificate, CertificateStatus newStatus) {
        if (certificate.getStatus() != newStatus) {
            statusChangeHandler.handleStatusChange(certificate, newStatus);
            certificate.setStatus(newStatus);
            certificate.setUpdatedAt(new Date());
            certificateRepository.save(certificate);
        }
        if (certificate.isAlive()) {
            scheduler.scheduleCheckStatusJob(certificate);
        }
    }

    @Transactional
    public void removeCertificate(User user, Long id) {
        certificateRepository.findById(id).ifPresent(certificate -> {
            logger.info("Found certificate {}", certificate);
            user.getCertificates().remove(certificate);
            userRepository.save(user);
            certificate.getUsers().remove(user);
            if (certificate.getUsers().isEmpty()) {
                logger.info("Certificate {} has no users, deleting", certificate);
                certificateRepository.delete(certificate);
            } else {
                certificateRepository.save(certificate);
            }
        });
    }

    private Certificate findOrCreate(String number, CertificateType type) throws InvalidCertificate {
        return certificateRepository.findByNumberAndType(number, type).orElseGet(() -> {
            Certificate certificate = Certificate.create(type);
            certificate.setNumber(number);
            return certificate;
        });
    }

}
