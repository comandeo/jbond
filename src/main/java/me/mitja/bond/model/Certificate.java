package me.mitja.bond.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import me.mitja.bond.exception.InvalidCertificate;

import javax.persistence.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Entity
@Table(
        name = "certificates",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"number", "type"})}
)
public class Certificate {

    private static final Pattern patentPattern = Pattern.compile("^\\d{7}$");

    private static final Pattern trademarkPattern = Pattern.compile("^\\d{6}$");

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private String number;

    @Column
    private CertificateStatus status = CertificateStatus.UNKNOWN;

    @Column
    private CertificateType type;

    @Column
    private Date updatedAt = null;

    @ManyToMany(mappedBy = "certificates", fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<User> users = new HashSet<>();

    @OneToMany(mappedBy = "certificate")
    private Set<StatusChangeEvent> statusChangeEvents = new HashSet<>();

    public static Certificate create(CertificateType type) {
        return new Certificate(type);
    }

    public Certificate() {
    }

    public Certificate(CertificateType type) {
        this.type = type;
    }

    public CertificateStatus getStatus() {
        return status;
    }

    public void setStatus(CertificateStatus status) {
        this.status = status;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) throws InvalidCertificate {
        this.number = number;
        validateState();
    }

    public CertificateType getType() {
        return type;
    }

    public void setType(CertificateType type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public boolean isDead() {
        return status == CertificateStatus.DEAD;
    }

    public boolean isAlive() {
        return !isDead();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Certificate that = (Certificate) o;
        return Objects.equals(number, that.number) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, type);
    }

    @Override
    public String toString() {
        return "Certificate{" +
                "number='" + number + '\'' +
                ", type=" + type +
                '}';
    }

    private static Optional<Pattern> getPattern(CertificateType type) {
        switch (type) {
            case RU_PATENT:
                return Optional.of(patentPattern);
            case RU_TRADEMARK:
                return Optional.of(trademarkPattern);
            default:
                return Optional.empty();
        }
    }

    private void validateState() throws InvalidCertificate {
        getPattern(getType()).ifPresent(pattern -> {
            Matcher matcher = pattern.matcher(getNumber());
            if (!matcher.matches()) {
                throw new InvalidCertificate("Invalid number");
            }
        });
    }

}
