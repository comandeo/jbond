package me.mitja.bond.model;

public enum CertificateStatus {
    UNKNOWN,
    VALID,
    VALID_BUT_CAN_BE_INVALIDATED,
    INVALID_BUT_CAN_BE_RESTORED,
    DEAD
}
