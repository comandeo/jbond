package me.mitja.bond.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class StatusChangeEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    private CertificateStatus oldStatus;

    @Column
    private CertificateStatus newStatus;

    @Column
    private Date createdAt = new Date();

    @ManyToOne
    @JoinColumn(name = "certificate_id")
    private Certificate certificate;

    public static StatusChangeEvent create(Certificate certificate, CertificateStatus newStatus) {
        StatusChangeEvent event = new StatusChangeEvent();
        event.setCertificate(certificate);
        event.setOldStatus(certificate.getStatus());
        event.setNewStatus(newStatus);
        return event;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CertificateStatus getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(CertificateStatus oldStatus) {
        this.oldStatus = oldStatus;
    }

    public CertificateStatus getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(CertificateStatus newStatus) {
        this.newStatus = newStatus;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatusChangeEvent that = (StatusChangeEvent) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
