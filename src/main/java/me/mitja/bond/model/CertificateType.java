package me.mitja.bond.model;

public enum CertificateType {
    RU_PATENT,
    EA_PATENT,
    RU_TRADEMARK
}
