package me.mitja.bond.exception;

public class InvalidCertificate extends Error {
    public InvalidCertificate(String message) {
        super(message);
    }
}
