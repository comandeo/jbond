package me.mitja.bond.job;

import me.mitja.bond.model.CertificateStatus;
import me.mitja.bond.parser.Parser;
import me.mitja.bond.parser.ParserFactory;
import me.mitja.bond.service.CertificateService;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
@DisallowConcurrentExecution
public class UpdateCertificateStatusJob extends QuartzJobBean {

    static final String certificateIdKey = "certificateId";

    private final CertificateService certificateService;

    private final ParserFactory parserFactory;

    @Autowired
    public UpdateCertificateStatusJob(CertificateService certificateService, ParserFactory parserFactory) {
        this.certificateService = certificateService;
        this.parserFactory = parserFactory;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        JobDataMap jobDataMap = context.getMergedJobDataMap();
        Long certificateId = jobDataMap.getLong(certificateIdKey);
        certificateService.getIfAlive(certificateId).ifPresent( certificate -> {
            final Parser parser = parserFactory.createParser(certificate.getType());
            final CertificateStatus newStatus = parser.getStatus(certificate.getNumber());
            certificateService.updateStatus(certificate, newStatus);
        });
    }
}
