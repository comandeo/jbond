package me.mitja.bond.job;

import me.mitja.bond.model.StatusChangeEvent;
import me.mitja.bond.model.User;
import me.mitja.bond.repository.StatusChangeEventRepository;
import me.mitja.bond.service.EmailNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class EventProcessorJob {

    private final StatusChangeEventRepository repository;

    private final EmailNotificationService emailNotificationService;

    @Autowired
    public EventProcessorJob(StatusChangeEventRepository repository, EmailNotificationService emailNotificationService) {
        this.repository = repository;
        this.emailNotificationService = emailNotificationService;
    }

    @Scheduled(cron = "0 0 6 * * *")
    public void processStatusChangeEvents() {
        final Iterable<StatusChangeEvent> allEvents = repository.findAll();
        Map<User, List<StatusChangeEvent>> eventMap = StreamSupport
                .stream(allEvents.spliterator(), false)
                .flatMap(event -> event
                        .getCertificate()
                        .getUsers()
                        .stream()
                        .map(user -> new StatusChangeNotification(user, event)))
                .collect(Collectors.groupingBy(StatusChangeNotification::getUser))
                .entrySet()
                .stream()
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey,
                                e -> e.getValue().stream().map(StatusChangeNotification::getEvent).collect(Collectors.toList())
                        )
                );

        emailNotificationService.sendStatusChangeNotifications(eventMap);
        repository.deleteAll(allEvents);
    }

    public static class StatusChangeNotification {
        final User user;

        final StatusChangeEvent event;

        StatusChangeNotification(User user, StatusChangeEvent event) {
            this.user = user;
            this.event = event;
        }

        public User getUser() {
            return user;
        }

        StatusChangeEvent getEvent() {
            return event;
        }
    }
}
