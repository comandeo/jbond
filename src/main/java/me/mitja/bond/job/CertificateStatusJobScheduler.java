package me.mitja.bond.job;

import me.mitja.bond.model.Certificate;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

@Component
public class CertificateStatusJobScheduler {

    private static final Logger logger = LoggerFactory.getLogger(CertificateStatusJobScheduler.class);

    private static final Random random = new Random();

    private Scheduler scheduler;

    @Autowired
    public CertificateStatusJobScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public void scheduleGetStatusJob(Certificate certificate) {
        scheduleUpdateJob(certificate, getFirstUpdateJobStartAt());
    }

    public void scheduleCheckStatusJob(Certificate certificate) {
        scheduleUpdateJob(certificate, getRecurringUpdateJobStartAt());
    }

    private void scheduleUpdateJob(Certificate certificate, Date at) {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(UpdateCertificateStatusJob.certificateIdKey, certificate.getId());
        final JobDetail jobDetail = JobBuilder.newJob(UpdateCertificateStatusJob.class)
                .withIdentity(certificate.toString(), "status-update-jobs")
                .withDescription("Update certificate status")
                .usingJobData(jobDataMap)
                .storeDurably()
                .build();
        final SimpleTrigger trigger = TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(jobDetail.getKey().getName(), "status-update-triggers")
                .withDescription("Update status Trigger")
                .startAt(at)
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
        try {
            if (!scheduler.checkExists(jobDetail.getKey())) {
                scheduler.scheduleJob(jobDetail, trigger);
            }
            logger.info("Scheduling update job for certificate id {} at {}", certificate.getId(), at);
        } catch (SchedulerException e) {
            logger.error("Cannot schedule update job", e);
        }
    }

    private Date getFirstUpdateJobStartAt() {
        final int minimumMinutes = 3;
        final int additionalMinutes = random.nextInt(7);
        java.util.Calendar date = java.util.Calendar.getInstance();
        date.add(java.util.Calendar.MINUTE, minimumMinutes + additionalMinutes);
        return date.getTime();
    }

    private Date getRecurringUpdateJobStartAt() {
        java.util.Calendar date = java.util.Calendar.getInstance();
        date.add(Calendar.WEEK_OF_MONTH, 1);
        return date.getTime();
    }
}
