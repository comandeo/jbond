package me.mitja.bond.ui.model;

import me.mitja.bond.model.Certificate;
import org.springframework.context.MessageSource;

import java.util.Date;
import java.util.Locale;

public class CertificateModel {
    private final Certificate certificate;

    private final MessageSource messageSource;

    private final Locale locale;

    public CertificateModel(Certificate certificate, MessageSource messageSource, Locale locale) {
        this.certificate = certificate;
        this.messageSource = messageSource;
        this.locale = locale;
    }

    public String getType() {
        switch (certificate.getType()) {
            case RU_PATENT:
                return messageSource.getMessage("certificate.type.RU_PATENT", null, locale);
            case EA_PATENT:
                return messageSource.getMessage("certificate.type.EA_PATENT", null, locale);
            case RU_TRADEMARK:
                return messageSource.getMessage("certificate.type.RU_TRADEMARK", null, locale);
        }
        return messageSource.getMessage("certificate.type.UNKNOWN", null, locale);
    }

    public String getNumber() {
        return certificate.getNumber();
    }

    public String getStatus() {
        switch (certificate.getStatus()) {
            case UNKNOWN:
                return messageSource.getMessage("certificate.status.UNKNOWN", null, locale);
            case VALID:
                return messageSource.getMessage("certificate.status.VALID", null, locale);
            case VALID_BUT_CAN_BE_INVALIDATED:
                return messageSource.getMessage("certificate.status.VALID_BUT_CAN_BE_INVALIDATED", null, locale);
            case INVALID_BUT_CAN_BE_RESTORED:
                return messageSource.getMessage("certificate.status.INVALID_BUT_CAN_BE_RESTORED", null, locale);
            case DEAD:
                return messageSource.getMessage("certificate.status.DEAD", null, locale);
        }
        return messageSource.getMessage("certificate.status.UNKNOWN", null, locale);
    }

    public Date getUpdatedAt() {
        return certificate.getUpdatedAt();
    }

}
