package me.mitja.bond.ui.model;

import me.mitja.bond.model.User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class ProfileModel {
    @NotNull
    private String email;

    @Positive
    private int maxCertificates;


    public String getEmail() {
        return email;
    }

    public int getMaxCertificates() {
        return maxCertificates;
    }

    public void setMaxCertificates(int maxCertificates) {
        this.maxCertificates = maxCertificates;
    }

    public static ProfileModel createFromUser(User user) {
        final ProfileModel profileModel = new ProfileModel();
        profileModel.email = user.getEmail();
        profileModel.maxCertificates = user.getMaxCertificates();
        return profileModel;
    }
}
