package me.mitja.bond.ui.controller;

import me.mitja.bond.security.CurrentUserHolder;
import me.mitja.bond.service.UserService;
import me.mitja.bond.ui.model.ProfileModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class ProfilesController {
    private final CurrentUserHolder currentUserHolder;

    private final UserService userService;

    @Autowired
    public ProfilesController(CurrentUserHolder currentUserHolder, UserService userService) {
        this.currentUserHolder = currentUserHolder;
        this.userService = userService;
    }

    @GetMapping("/me")
    public String show(Model model) {
        model.addAttribute("profile", ProfileModel.createFromUser(currentUserHolder.getCurrentUser()));
        return "me/show";
    }

    @PostMapping("/me")
    public String update(@Valid ProfileModel profile, BindingResult result, Model model) {
        userService.requestMaxCertificates(currentUserHolder.getCurrentUser(), profile.getMaxCertificates());
        model.addAttribute("profile", profile);
        return "me/update";
    }
}
