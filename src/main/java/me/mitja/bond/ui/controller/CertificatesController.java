package me.mitja.bond.ui.controller;

import me.mitja.bond.exception.InvalidCertificate;
import me.mitja.bond.exception.TooManyCertificatesException;
import me.mitja.bond.model.Certificate;
import me.mitja.bond.model.CertificateType;
import me.mitja.bond.ui.model.CertificateModel;
import me.mitja.bond.security.CurrentUserHolder;
import me.mitja.bond.service.CertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class CertificatesController {

    private static final int PAGE_SIZE = 20;

    private final CertificateService certificateService;

    private CurrentUserHolder currentUserHolder;

    private MessageSource messageSource;

    @Autowired
    public CertificatesController(
        CertificateService certificateService,
        CurrentUserHolder currentUserHolder,
        MessageSource messageSource
    ) {
        this.certificateService = certificateService;
        this.currentUserHolder = currentUserHolder;
        this.messageSource = messageSource;
    }

    @GetMapping("/certificates")
    public ModelAndView index(
        @RequestParam(value = "page", defaultValue = "1") Integer page
    ) {
        ModelAndView modelAndView = new ModelAndView();
        setupModelAndView(modelAndView, page);
        return modelAndView;
    }

    @PostMapping("/certificates")
    public ModelAndView create(
        @RequestParam("number") String number,
        @RequestParam("type") CertificateType type
    ) {
        ModelAndView modelAndView = new ModelAndView();
        try {
            certificateService.createCertificate(currentUserHolder.getCurrentUser(), number, type);
            modelAndView.setViewName("redirect:/certificates");
            return modelAndView;
        } catch (TooManyCertificatesException e) {
            modelAndView.addObject("error", "Too many certificates");
        } catch (InvalidCertificate invalidCertificate) {
            modelAndView.addObject("error", "Invalid certificate");
        }
        setupModelAndView(modelAndView, 1);
        return modelAndView;
    }

    @PostMapping("/certificates/batch")
    public ModelAndView createBatch(
        @RequestPart("file") MultipartFile file,
        @RequestParam("type") CertificateType type
    ) throws IOException {
        ModelAndView modelAndView = new ModelAndView();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                file.getInputStream())
            );
            final List<String> numbers = br
                .lines()
                .collect(Collectors.toList());
            certificateService.createCertificates(currentUserHolder.getCurrentUser(), numbers, type);
            modelAndView.setViewName("redirect:/certificates");
            return modelAndView;
        } catch (TooManyCertificatesException e) {
            modelAndView.addObject("error", "Too many certificates");
        } catch (InvalidCertificate invalidCertificate) {
            modelAndView.addObject("error", "Invalid certificate");
        }
        setupModelAndView(modelAndView, 1);
        return modelAndView;
    }

    private void setupModelAndView(@NonNull ModelAndView modelAndView, int pageNumber) {
        final Page<Certificate> page = certificateService
                .getCertificates(currentUserHolder.getCurrentUser(), pageNumber - 1, PAGE_SIZE);
        List<CertificateModel> certificates = page
            .stream()
            .map(certificate -> new CertificateModel(
                certificate,
                messageSource,
                LocaleContextHolder.getLocale()
            ))
            .collect(Collectors.toList());
        modelAndView.addObject("user", currentUserHolder.getCurrentUser());
        modelAndView.addObject("certificates", certificates);
        modelAndView.addObject("certificatesCount", page.getTotalElements());
        modelAndView.addObject("currentPage", pageNumber);
        int[] pages = IntStream.range(1, page.getTotalPages() + 1).toArray();
        modelAndView.addObject("pages", pages);
        modelAndView.setViewName("certificates/index");
    }

}
