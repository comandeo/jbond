package me.mitja.bond.security;

import me.mitja.bond.security.ui.RegistrationForm;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RegistrationFormTest {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void testValidations() {
        RegistrationForm subject = new RegistrationForm();
        Set<ConstraintViolation<RegistrationForm>> violations = validator.validate(subject);
        assertEquals(3, violations.size());
        subject.setEmail("some@email");
        subject.setPassword("123");
        subject.setPasswordRepeat("456");
        violations = validator.validate(subject);
        assertEquals(1, violations.size());
        subject.setPasswordRepeat("123");
        violations = validator.validate(subject);
        assertTrue(violations.isEmpty());
    }
}
