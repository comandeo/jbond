package me.mitja.bond.job;

import me.mitja.bond.model.Certificate;
import me.mitja.bond.model.CertificateStatus;
import me.mitja.bond.model.StatusChangeEvent;
import me.mitja.bond.model.User;
import me.mitja.bond.repository.StatusChangeEventRepository;
import me.mitja.bond.service.EmailNotificationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class EventProcessorJobTest {

    @Captor
    ArgumentCaptor<Map<User, List<StatusChangeEvent>>> argumentCaptor;

    @Mock
    StatusChangeEventRepository repository;

    @Mock
    EmailNotificationService emailNotificationService;

    List<StatusChangeEvent> events = new ArrayList<>();


    @Test
    public void processStatusChangeEvents() {
        final User user1 = new User();
        user1.setId(1);
        user1.setEmail("user1@email.com");
        final User user2 = new User();
        user1.setId(2);
        user1.setEmail("user2@email.com");

        final Certificate certificate1 = new Certificate();
        certificate1.getUsers().add(user1);
        certificate1.getUsers().add(user2);
        final StatusChangeEvent event1 = StatusChangeEvent.create(certificate1, CertificateStatus.VALID);
        events.add(event1);

        final Certificate certificate2 = new Certificate();
        certificate2.getUsers().add(user1);
        final StatusChangeEvent event2 = StatusChangeEvent.create(certificate2, CertificateStatus.DEAD);
        events.add(event2);

        when(repository.findAll()).thenReturn(events);

        final EventProcessorJob subject = new EventProcessorJob(repository, emailNotificationService);
        subject.processStatusChangeEvents();

        verify(emailNotificationService, times(1)).sendStatusChangeNotifications(argumentCaptor.capture());
        final Map<User, List<StatusChangeEvent>> eventMap = argumentCaptor.getValue();
        assertEquals(2, eventMap.size());

        final List<StatusChangeEvent> eventsForUser1 = eventMap.get(user1);
        assertEquals(2, eventsForUser1.size());
        assertEquals(event1, eventsForUser1.get(0));
        assertEquals(event2, eventsForUser1.get(1));

        final List<StatusChangeEvent> eventsForUser2 = eventMap.get(user2);
        assertEquals(1, eventsForUser2.size());
        assertEquals(event1, eventsForUser2.get(0));

        verify(repository, times(1)).deleteAll(events);
    }
}
