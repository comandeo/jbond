package me.mitja.bond.job;

import me.mitja.bond.model.Certificate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class CertificateStatusJobSchedulerTest {

    @Mock
    Scheduler scheduler;

    @Captor
    ArgumentCaptor<JobDetail> jobDetailArgumentCaptor;

    @Captor
    ArgumentCaptor<Trigger> triggerArgumentCaptor;

    private CertificateStatusJobScheduler subject;

    private Certificate certificate;

    @Before
    public void setUp() {
        subject = new CertificateStatusJobScheduler(scheduler);
        certificate = new Certificate();
        certificate.setId(123);
    }

    @Test
    public void scheduleGetStatusJob() throws SchedulerException {
        final long now = new Date().getTime();
        subject.scheduleGetStatusJob(certificate);
        verify(scheduler, times(1)).scheduleJob(jobDetailArgumentCaptor.capture(), triggerArgumentCaptor.capture());
        final long certificateId = jobDetailArgumentCaptor.getValue().getJobDataMap().getLongValue(UpdateCertificateStatusJob.certificateIdKey);
        assertEquals(certificate.getId(), certificateId);
        long scheduledIn = triggerArgumentCaptor.getValue().getStartTime().getTime() - now;
        assertTrue("Should be scheduled after three minutes but scheduled in " + scheduledIn / 1000 / 60, scheduledIn >= 3 * 60 * 1000);
        assertTrue("Should be scheduled before eight minutes but scheduled in " + scheduledIn / 1000 / 60,scheduledIn <= 8 * 60 * 1000);
    }

    @Test
    public void scheduleCheckStatusJob() throws SchedulerException {
        final long now = new Date().getTime();
        subject.scheduleCheckStatusJob(certificate);
        verify(scheduler, times(1)).scheduleJob(jobDetailArgumentCaptor.capture(), triggerArgumentCaptor.capture());
        final long certificateId = jobDetailArgumentCaptor.getValue().getJobDataMap().getLongValue(UpdateCertificateStatusJob.certificateIdKey);
        assertEquals(certificate.getId(), certificateId);
        long scheduledIn = triggerArgumentCaptor.getValue().getStartTime().getTime() - now;
        assertTrue("Should be scheduled in seven days but scheduled in " + scheduledIn / 1000 / 60 / 24,scheduledIn >=  7 * 24 * 60 * 60 * 1000);

    }
}
