package me.mitja.bond.job;

import me.mitja.bond.model.Certificate;
import me.mitja.bond.model.CertificateStatus;
import me.mitja.bond.model.CertificateType;
import me.mitja.bond.parser.Parser;
import me.mitja.bond.parser.ParserFactory;
import me.mitja.bond.service.CertificateService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class UpdateCertificateStatusJobTest {

    UpdateCertificateStatusJob subject;

    @Mock
    CertificateService certificateService;

    @Mock
    ParserFactory parserFactory;

    @Mock
    Parser parser;

    @Mock
    JobExecutionContext context;

    @Mock
    JobDataMap jobDataMap;

    long certificateId = 123L;

    String number = "2123456";

    @Before
    public void setUp() {
        when(jobDataMap.getLong(UpdateCertificateStatusJob.certificateIdKey)).thenReturn(certificateId);
        when(context.getMergedJobDataMap()).thenReturn(jobDataMap);
        when(parserFactory.createParser(any())).thenReturn(parser);
        subject = new UpdateCertificateStatusJob(certificateService, parserFactory);
    }

    @Test
    public void executeInternalUpdateStatus() throws JobExecutionException {
        final Certificate certificate = new Certificate();
        certificate.setId(certificateId);
        certificate.setType(CertificateType.RU_PATENT);
        certificate.setNumber(number);
        when(certificateService.getIfAlive(certificateId)).thenReturn(Optional.of(certificate));
        when(parser.getStatus(number)).thenReturn(CertificateStatus.VALID);
        subject.executeInternal(context);
        verify(certificateService, times(1)).updateStatus(certificate, CertificateStatus.VALID);
    }

    @Test
    public void executeInternalDoesNotUpdateStatusForDead() throws JobExecutionException {
        when(certificateService.getIfAlive(certificateId)).thenReturn(Optional.empty());
        when(parser.getStatus(number)).thenThrow(new IllegalArgumentException());
        subject.executeInternal(context);
        verify(certificateService, never()).updateStatus(any(), any());
        verify(parserFactory, never()).createParser(any());
    }
}
