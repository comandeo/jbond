package me.mitja.bond.service;

import me.mitja.bond.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class EmailNotificationServiceTest {

    @Mock
    JavaMailSender emailSender;

    @Mock
    MessageSource messageSource;

    @Captor
    ArgumentCaptor<SimpleMailMessage> messageArgumentCaptor;

    EmailNotificationService subject;

    private User user1;

    private User user2;

    private Certificate certificate1;

    private Certificate certificate2;

    @Before
    public void setUp() {
        user1 = new User();
        user1.setEmail("user1@email.com");
        user2 = new User();
        user2.setEmail("user2@email.com");
        certificate1 = new Certificate();
        certificate1.setType(CertificateType.RU_PATENT);
        certificate1.setNumber("2123456");
        certificate1.setStatus(CertificateStatus.VALID_BUT_CAN_BE_INVALIDATED);
        certificate2 = new Certificate();
        certificate2.setType(CertificateType.RU_PATENT);
        certificate2.setNumber("2123456");
        certificate2.setStatus(CertificateStatus.INVALID_BUT_CAN_BE_RESTORED);
        subject = new EmailNotificationService(emailSender, messageSource);
    }

    @Test
    public void sendStatusChangeNotifications() {
        Map<User, List<StatusChangeEvent>> map = new HashMap<>();
        map.put(user1, new ArrayList<>());
        map.get(user1).add(StatusChangeEvent.create(certificate1, CertificateStatus.DEAD));
        map.get(user1).add(StatusChangeEvent.create(certificate2, CertificateStatus.VALID));
        map.put(user2, new ArrayList<>());
        map.get(user2).add(StatusChangeEvent.create(certificate1, CertificateStatus.DEAD));
        subject.sendStatusChangeNotifications(map);
        verify(emailSender, times(2)).send(messageArgumentCaptor.capture());
        List<SimpleMailMessage> emails = messageArgumentCaptor.getAllValues();
        SimpleMailMessage user1Email = emails.get(1);
        assertArrayEquals(new String[] { user1.getEmail() }, user1Email.getTo());

        SimpleMailMessage user2Email = emails.get(0);
        assertArrayEquals(new String[] { user2.getEmail() }, user2Email.getTo());
    }

}
