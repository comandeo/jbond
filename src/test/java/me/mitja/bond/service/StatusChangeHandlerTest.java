package me.mitja.bond.service;

import me.mitja.bond.model.Certificate;
import me.mitja.bond.model.CertificateStatus;
import me.mitja.bond.model.CertificateType;
import me.mitja.bond.model.StatusChangeEvent;
import me.mitja.bond.repository.StatusChangeEventRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class StatusChangeHandlerTest {

    StatusChangeHandler subject;

    @Mock
    StatusChangeEventRepository repository;

    @Captor
    ArgumentCaptor<StatusChangeEvent> eventArgumentCaptor;

    Certificate certificate;

    @Before
    public void setUp() {
        subject = new StatusChangeHandler(repository);
        certificate = new Certificate();
        certificate.setType(CertificateType.RU_PATENT);
        certificate.setNumber("2123456");
        certificate.setStatus(CertificateStatus.VALID);
    }


    @Test
    public void handleStatusChange() {
        final CertificateStatus oldStatus = certificate.getStatus();
        final CertificateStatus newStatus = CertificateStatus.INVALID_BUT_CAN_BE_RESTORED;
        subject.handleStatusChange(certificate, newStatus);
        verify(repository).save(eventArgumentCaptor.capture());
        final StatusChangeEvent event = eventArgumentCaptor.getValue();
        assertEquals(oldStatus, event.getOldStatus());
        assertEquals(newStatus, event.getNewStatus());
        assertEquals(certificate, event.getCertificate());
    }

    @Test
    public void handleStatusChangeSameStatus() {
        final CertificateStatus oldStatus = certificate.getStatus();
        final CertificateStatus newStatus = oldStatus;
        subject.handleStatusChange(certificate, newStatus);
        verify(repository, never()).save(any());

    }

    @Test
    public void handleStatusChangeUnknownStatus() {
        certificate.setStatus(CertificateStatus.UNKNOWN);
        final CertificateStatus oldStatus = certificate.getStatus();
        final CertificateStatus newStatus = CertificateStatus.VALID;
        subject.handleStatusChange(certificate, newStatus);
        verify(repository, never()).save(any());

    }
}
