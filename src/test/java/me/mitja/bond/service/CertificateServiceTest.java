package me.mitja.bond.service;

import me.mitja.bond.exception.InvalidCertificate;
import me.mitja.bond.job.CertificateStatusJobScheduler;
import me.mitja.bond.model.Certificate;
import me.mitja.bond.model.CertificateStatus;
import me.mitja.bond.model.CertificateType;
import me.mitja.bond.model.User;
import me.mitja.bond.repository.CertificateRepository;
import me.mitja.bond.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class CertificateServiceTest {

    @Mock
    CertificateRepository certificateRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    CertificateStatusJobScheduler scheduler;

    @Mock
    StatusChangeHandler statusChangeHandler;

    @Captor
    ArgumentCaptor<Certificate> certificateArgumentCaptor;

    CertificateService subject;

    Certificate aliveCertificate;

    Certificate deadCertificate;

    @Before
    public void setUp() {
        aliveCertificate = new Certificate();
        aliveCertificate.setId(1L);
        aliveCertificate.setStatus(CertificateStatus.VALID);
        deadCertificate = new Certificate();
        aliveCertificate.setId(2L);
        deadCertificate.setStatus(CertificateStatus.DEAD);
        subject = new CertificateService(certificateRepository, userRepository, scheduler, statusChangeHandler);
    }


    @Test
    public void getIfAlive() {
        when(certificateRepository.findById(aliveCertificate.getId())).thenReturn(Optional.of(aliveCertificate));
        assertEquals(Optional.of(aliveCertificate), subject.getIfAlive(aliveCertificate.getId()));
        when(certificateRepository.findById(deadCertificate.getId())).thenReturn(Optional.empty());
        assertEquals(Optional.empty(), subject.getIfAlive(deadCertificate.getId()));
    }

    @Test
    public void getCertificates() {
    }

    @Test
    public void createValidCertificate() {
        final User user = new User();
        final String number = "2123456";
        subject.createCertificate(user, number, CertificateType.RU_PATENT);
        assertEquals(1, user.getCertificates().size());
        verify(certificateRepository).save(certificateArgumentCaptor.capture());
        final Certificate savedCertificate = certificateArgumentCaptor.getValue();
        assertEquals(number, savedCertificate.getNumber());
        verify(userRepository).save(user);
        verify(scheduler).scheduleGetStatusJob(savedCertificate);
    }

    @Test
    public void createInvalidCertificate() {
        final User user = new User();
        final String number = "2";
        try {
            subject.createCertificate(user, number, CertificateType.RU_PATENT);
            fail();
        } catch (InvalidCertificate e) {

        }
        assertEquals(0, user.getCertificates().size());
        verify(certificateRepository, never()).save(any());
        verify(userRepository, never()).save(any());
        verify(scheduler, never()).scheduleGetStatusJob(any());

    }

    @Test
    public void updateStatusToNew() {
        final CertificateStatus newStatus = CertificateStatus.INVALID_BUT_CAN_BE_RESTORED;
        subject.updateStatus(aliveCertificate, newStatus);
        verify(statusChangeHandler).handleStatusChange(aliveCertificate, newStatus);
        assertEquals(newStatus, aliveCertificate.getStatus());
        verify(certificateRepository).save(aliveCertificate);
        assertNotNull(aliveCertificate.getUpdatedAt());
        verify(scheduler).scheduleCheckStatusJob(aliveCertificate);
    }

    @Test
    public void updateStatusToSame() {
        subject.updateStatus(aliveCertificate, aliveCertificate.getStatus());
        verify(scheduler).scheduleCheckStatusJob(aliveCertificate);
    }

    @Test
    public void updateStatusToDead() {
        final CertificateStatus newStatus = CertificateStatus.DEAD;
        subject.updateStatus(aliveCertificate, newStatus);
        verify(statusChangeHandler).handleStatusChange(aliveCertificate, newStatus);
        assertEquals(newStatus, aliveCertificate.getStatus());
        verify(certificateRepository).save(aliveCertificate);
        verify(scheduler, never()).scheduleCheckStatusJob(aliveCertificate);
    }

}
