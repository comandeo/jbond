package me.mitja.bond.parser;

import me.mitja.bond.model.CertificateStatus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class FipsInventionPatentParserTests {

    @Mock
    FipsDocumentFetcher documentFetcher;

    @Test
    public void testDeadStatus() throws IOException {
        final String number = "2123456";
        when(documentFetcher.fetchHtmlDocument(number, "RUPAT")).thenReturn(getDocument(number));
        FipsInventionPatentParser subject = new FipsInventionPatentParser(documentFetcher);
        assertEquals(CertificateStatus.DEAD, subject.getStatus(number));
    }

    @Test
    public void testValidStatus() throws IOException {
        final String number = "2523457";
        when(documentFetcher.fetchHtmlDocument(number, "RUPAT")).thenReturn(getDocument(number));
        FipsInventionPatentParser subject = new FipsInventionPatentParser(documentFetcher);
        assertEquals(CertificateStatus.VALID, subject.getStatus(number));
    }

    @Test
    public void testInvalidButCanBeRestoredStatus() throws IOException {
        final String number = "2523459";
        when(documentFetcher.fetchHtmlDocument(number, "RUPAT")).thenReturn(getDocument(number));
        FipsInventionPatentParser subject = new FipsInventionPatentParser(documentFetcher);
        assertEquals(CertificateStatus.INVALID_BUT_CAN_BE_RESTORED, subject.getStatus(number));
    }


    private Document getDocument(String number) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(number + ".htm")).getFile());
        return Jsoup.parse(file, "windows-1251", "http://www1.fips.ru/fips_servl/fips_servlet");
    }
}
